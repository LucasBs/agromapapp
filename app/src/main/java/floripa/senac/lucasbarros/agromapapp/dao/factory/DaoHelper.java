package floripa.senac.lucasbarros.agromapapp.dao.factory;

import android.content.Context;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

public class DaoHelper<T> {
    private Class className;
    private static MyORMLiteHelper minstance = null;

    public DaoHelper(Context context, Class className) {
        this.className = className;
        if (minstance == null){
            minstance = new MyORMLiteHelper(context);
        }
    }

    public Dao<T, Integer> getDao(){
        try {
            return minstance.getDao(this.className);
        } catch (SQLException e) {
            return null;
        }
    }
}
