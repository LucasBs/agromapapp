package floripa.senac.lucasbarros.agromapapp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import floripa.senac.lucasbarros.agromapapp.R;
import floripa.senac.lucasbarros.agromapapp.control.PropriedadeControl;

public class PerfilActivity extends AppCompatActivity {

    private PropriedadeControl propriedadeControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        propriedadeControl = new PropriedadeControl(this);
    }

    public void cadastrar(View view){
        propriedadeControl.cadastrarAction();
    }
}
