package floripa.senac.lucasbarros.agromapapp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import floripa.senac.lucasbarros.agromapapp.R;
import floripa.senac.lucasbarros.agromapapp.control.ListarController;

public class ListarPropriedadeActivity extends AppCompatActivity {


    private ListarController listarController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_propriedade);
        listarController = new ListarController(this);
    }


    public  void  cadastrar(View view){
        listarController.cadastrar();
    }

}
