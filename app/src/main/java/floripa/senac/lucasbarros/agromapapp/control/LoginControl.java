package floripa.senac.lucasbarros.agromapapp.control;

import android.app.Activity;
import android.content.Entity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;
import floripa.senac.lucasbarros.agromapapp.R;
import floripa.senac.lucasbarros.agromapapp.Utils.Utils;
import floripa.senac.lucasbarros.agromapapp.model.User;
import floripa.senac.lucasbarros.agromapapp.view.CadastroActivity;
import floripa.senac.lucasbarros.agromapapp.view.ListarPropriedadeActivity;
import floripa.senac.lucasbarros.agromapapp.view.PerfilActivity;

public class LoginControl {

    private Activity activity;
    private EditText login;
    private  EditText password;
    private User user;

    private AsyncHttpClient client = new AsyncHttpClient();

    public LoginControl(Activity activity) {
        this.activity = activity;
        init();
    }

    public void init(){
        login = activity.findViewById(R.id.editNomeLogin);
        password = activity.findViewById(R.id.editPassLogin);

        user = new User();
        user.setLogin(login.getText().toString());
        user.setPassword(password.getText().toString());

        client.addHeader("user-agent","Mozilla Chrome");
    }

    public void actionLogar() {
        user.setLogin(login.getText().toString());
        user.setPassword(password.getText().toString());

        Gson gson = new Gson();
        StringEntity entity = null;
        try {
            entity = new StringEntity(gson.toJson(user));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,"application/json"));

     client.post(this.activity, Utils.URL+"/api/user/login",entity,"application/json",new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String strings = new String(responseBody);
                Gson gson = new Gson();
                User user = gson.fromJson(strings,User.class);

                /*SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putLong("userId",user.getId());*/
                Toast.makeText(activity, "Logado", Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(activity, ListarPropriedadeActivity.class);
                intent.putExtra("user",user);
                activity.startActivity(intent);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String strings = new String(responseBody);
                Toast.makeText(activity, " status "+statusCode+" "+strings+" "+headers, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void cadastrarAction(){
        Intent it = new Intent(activity, CadastroActivity.class);
        activity.startActivity(it);
    }

}
