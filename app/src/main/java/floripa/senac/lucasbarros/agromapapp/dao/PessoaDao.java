package floripa.senac.lucasbarros.agromapapp.dao;

import android.content.Context;

import floripa.senac.lucasbarros.agromapapp.dao.factory.DaoHelper;
import floripa.senac.lucasbarros.agromapapp.model.Pessoa;

public class PessoaDao extends DaoHelper<Pessoa> {
    public PessoaDao(Context context) {
        super(context, Pessoa.class);
    }
}
