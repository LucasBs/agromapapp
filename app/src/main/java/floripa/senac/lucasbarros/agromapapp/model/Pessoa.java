package floripa.senac.lucasbarros.agromapapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;


@DatabaseTable(tableName = "pessoa")
public class Pessoa implements Serializable {

    @DatabaseField(allowGeneratedIdInsert = true,generatedId = true)
    private Long id;

    @DatabaseField(width = 60,canBeNull = false)
    private String nome;

    @DatabaseField(width = 60,canBeNull = false)
    private String sobrenome;

    @DatabaseField(width = 60,canBeNull = false)
    private String sexo;

    @DatabaseField(width = 60,canBeNull = false)
    private String cpf;

    @DatabaseField(width = 60,canBeNull = false)
    private String nascimento;

    @DatabaseField(width = 60,canBeNull = false)
    private String email;

    @DatabaseField(width = 60,canBeNull = false)
    private String login;

    @DatabaseField(width = 60,canBeNull = false)
    private String senha;

    @ForeignCollectionField(eager = true)
    private User user;

    public Pessoa() {
    }

    public Pessoa(String nome, String sobrenome, String sexo, String cpf, String nascimento, String email, String login, String senha, User user) {
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.sexo = sexo;
        this.cpf = cpf;
        this.nascimento = nascimento;
        this.email = email;
        this.login = login;
        this.senha = senha;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getNascimento() {
        return nascimento;
    }

    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }


}
