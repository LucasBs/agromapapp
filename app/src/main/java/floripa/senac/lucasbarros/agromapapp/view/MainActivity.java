package floripa.senac.lucasbarros.agromapapp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import floripa.senac.lucasbarros.agromapapp.R;
import floripa.senac.lucasbarros.agromapapp.control.LoginControl;

public class MainActivity extends AppCompatActivity {

    private LoginControl loginControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loginControl = new LoginControl(this);
    }

    public void logar(View view){
        loginControl.actionLogar();
    }

    public void cadastrar(View view){
        loginControl.cadastrarAction();
    }

}
