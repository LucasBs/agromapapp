package floripa.senac.lucasbarros.agromapapp.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import floripa.senac.lucasbarros.agromapapp.R;
import floripa.senac.lucasbarros.agromapapp.control.PessoaControl;

public class CadastroActivity extends AppCompatActivity {

    private PessoaControl pessoaControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        pessoaControl = new PessoaControl(this);
    }

    public void cadastrar(View view){
        pessoaControl.cadastrarAction();
    }



}
