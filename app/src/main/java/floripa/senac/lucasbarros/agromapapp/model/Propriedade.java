package floripa.senac.lucasbarros.agromapapp.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

@DatabaseTable(tableName = "propriedade")
public class Propriedade implements Serializable {
    @DatabaseField(allowGeneratedIdInsert = true,generatedId = true)
    private Long id;

    private String logradouro;
    private String cep;
    private String cidade;
    private String bairro;
    private  String uf;
    private  String numero;
    private String latitude;
    private String longitude;
    private String documento;
    private String cadastro;
    private String kmQuadrados;

    @ForeignCollectionField(eager = true)
    private User user;

    public Propriedade() {
    }

    public Propriedade(String logradouro, String cep, String cidade, String bairro, String uf, String latitude, String longitude, String documento, String cadastro) {
        this.logradouro = logradouro;
        this.cep = cep;
        this.cidade = cidade;
        this.bairro = bairro;
        this.uf = uf;
        this.latitude = latitude;
        this.longitude = longitude;
        this.documento = documento;
        this.cadastro = cadastro;

    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getCadastro() {
        return cadastro;
    }

    public void setCadastro(String cadastro) {
        this.cadastro = cadastro;
    }

    public String getKmQuadrados() {
        return kmQuadrados;
    }

    public void setKmQuadrados(String kmQuadrados) {
        this.kmQuadrados = kmQuadrados;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Logradouro: " + logradouro  + " Cep: " + cep  + " Bairro: " + bairro + " Uf: " + uf ;
    }
}
