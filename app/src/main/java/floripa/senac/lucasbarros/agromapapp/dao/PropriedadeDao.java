package floripa.senac.lucasbarros.agromapapp.dao;

import android.content.Context;

import floripa.senac.lucasbarros.agromapapp.dao.factory.DaoHelper;
import floripa.senac.lucasbarros.agromapapp.model.Propriedade;

public class PropriedadeDao extends DaoHelper<Propriedade> {
    public PropriedadeDao(Context context) {
        super(context, Propriedade.class);
    }
}
