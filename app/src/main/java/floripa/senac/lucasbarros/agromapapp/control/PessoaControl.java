package floripa.senac.lucasbarros.agromapapp.control;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;
import floripa.senac.lucasbarros.agromapapp.R;
import floripa.senac.lucasbarros.agromapapp.Utils.Utils;
import floripa.senac.lucasbarros.agromapapp.dao.PessoaDao;
import floripa.senac.lucasbarros.agromapapp.model.Pessoa;
import floripa.senac.lucasbarros.agromapapp.model.User;
import floripa.senac.lucasbarros.agromapapp.view.CadastroActivity;
import floripa.senac.lucasbarros.agromapapp.view.MainActivity;

public class PessoaControl {
    private Activity activity;
    private EditText editNome, editSobrenome, editSexo, editCpf, editEmail, editLogin, editSenha;
    private DatePicker editNascimento;
    private Pessoa pessoa;
    private User user;

    private AsyncHttpClient client = new AsyncHttpClient();

    public PessoaControl(Activity activity) {
        this.activity = activity;
        user = new User();
        pessoa = new Pessoa();
        initComponents();
    }

    private void initComponents() {
        editNome = activity.findViewById(R.id.editNome);
        editSobrenome = activity.findViewById(R.id.editSobrenome);
        editSexo = activity.findViewById(R.id.editSexo);
        editCpf = activity.findViewById(R.id.editCpf);
        editNascimento = activity.findViewById(R.id.editNascimento);
        editEmail = activity.findViewById(R.id.editEmail);
        editLogin = activity.findViewById(R.id.editLogin);
        editSenha = activity.findViewById(R.id.editSenha);

        client.addHeader("user-agent","Mozilla Chrome");
    }

    public void cadastrarAction(){
        user.setEmail(editEmail.getText().toString());
        user.setLogin(editLogin.getText().toString());
        user.setPassword(editSenha.getText().toString());



        Gson gson = new Gson();
        StringEntity entity = null;

        try {
            entity = new StringEntity(gson.toJson(user));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,"application/json"));

        client.post(this.activity, Utils.URL+"/api/user/add",entity,"application/json",new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String strings = new String(responseBody);
                Gson gson = new Gson();
                User user = gson.fromJson(strings,User.class);
                cadastrarPessoa(user);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String strings = new String(responseBody);
                Toast.makeText(activity, " status "+statusCode+" "+strings+" "+headers, Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void cadastrarPessoa(User user){
        pessoa.setCpf(editCpf.getText().toString());

        Date dt = getDateFromDatePicker(editNascimento);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate= formatter.format(dt.getTime());
        pessoa.setNascimento(strDate);
        pessoa.setNome(editNome.getText().toString());
        pessoa.setSobrenome(editSobrenome.getText().toString());
        pessoa.setSexo(editSexo.getText().toString());
        pessoa.setUser(user);

        Gson gson = new Gson();
        StringEntity entity = null;

        try {
            entity = new StringEntity(gson.toJson(pessoa));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,"application/json"));
        client.post(this.activity, Utils.URL+"/api/pessoa/add",entity,"application/json",new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String strings = new String(responseBody);
                /*Gson gson = new Gson();
                User user = gson.fromJson(strings,User.class);*/
                Toast.makeText(activity, "Cadastro Criado", Toast.LENGTH_SHORT).show();
                Intent it = new Intent(activity, MainActivity.class);
                activity.startActivity(it);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String strings = new String(responseBody);
                Toast.makeText(activity, " status "+statusCode+" "+strings+" "+headers, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static java.util.Date getDateFromDatePicker(DatePicker datePicker){
        int day = datePicker.getDayOfMonth()+1;
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

 }

