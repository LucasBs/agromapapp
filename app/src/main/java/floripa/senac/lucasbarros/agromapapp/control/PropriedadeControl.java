package floripa.senac.lucasbarros.agromapapp.control;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;
import floripa.senac.lucasbarros.agromapapp.R;
import floripa.senac.lucasbarros.agromapapp.Utils.Utils;
import floripa.senac.lucasbarros.agromapapp.model.Propriedade;
import floripa.senac.lucasbarros.agromapapp.model.User;
import floripa.senac.lucasbarros.agromapapp.view.ListarPropriedadeActivity;

public class PropriedadeControl {
    private Activity activity;
    private Propriedade propriedade;
    private User user;
    private EditText editLogradouro, editCep, editCidade, editBairro, editUf, editLatitude, editLongitude,editNumber, editDocumento, editCadastro, editKM;

    private AsyncHttpClient client = new AsyncHttpClient();

    public PropriedadeControl(Activity activity) {
        this.activity = activity;
        initComponents();
        user = (User) activity.getIntent().getSerializableExtra("userId");
        if(activity.getIntent().hasExtra("propriedade")){
            propriedade = (Propriedade) activity.getIntent().getSerializableExtra("propriedade");
            populate();
        }else{
            propriedade = new Propriedade();

        }


    }

    private void initComponents() {
        editLogradouro = activity.findViewById(R.id.editLogradouro);
        editCep = activity.findViewById(R.id.editCep);
        editCidade = activity.findViewById(R.id.editCidade);
        editBairro = activity.findViewById(R.id.editBairro);
        editUf = activity.findViewById(R.id.editUf);
        editLatitude = activity.findViewById(R.id.editLatitude);
        editLongitude = activity.findViewById(R.id.editLongitude);
        editDocumento = activity.findViewById(R.id.editDocumento);
        editKM = activity.findViewById(R.id.kmQuad);
        editNumber = activity.findViewById(R.id.numero);

        client.addHeader("user-agent","Mozilla Chrome");
    }

    private void cadastrarPropriedade(){
        propriedade.setBairro(editBairro.getText().toString());
        propriedade.setCep(editCep.getText().toString());
        propriedade.setCidade(editCidade.getText().toString());
        propriedade.setLatitude(editLatitude.getText().toString());
        propriedade.setLongitude(editLongitude.getText().toString());
        propriedade.setLogradouro(editLogradouro.getText().toString());
        propriedade.setUf(editUf.getText().toString());
        propriedade.setDocumento(editDocumento.getText().toString());
        propriedade.setKmQuadrados(editKM.getText().toString());
        propriedade.setNumero(editNumber.getText().toString());
        propriedade.setUser(user);
    }


    public void  populate(){
        propriedade.setUser(user);
        editBairro.setText(propriedade.getBairro());
        editCep.setText(propriedade.getCep());
        editCidade.setText(propriedade.getCidade());
        editLatitude.setText(propriedade.getLatitude());
        editLongitude.setText(propriedade.getLongitude());
        editLogradouro.setText(propriedade.getLogradouro());
        editUf.setText(propriedade.getUf());
        editDocumento.setText(propriedade.getDocumento());
        editKM.setText(propriedade.getKmQuadrados());
        editNumber.setText(propriedade.getNumero());
    }

    public void cadastrarAction(){
       cadastrarPropriedade();

        Gson gson = new Gson();
        StringEntity entity = null;

        try {
            entity = new StringEntity(gson.toJson(propriedade));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE,"application/json"));

        if(!activity.getIntent().hasExtra("propriedade")){

        client.post(this.activity, Utils.URL+"/api/propriedade/add",entity,"application/json",new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String strings = new String(responseBody);
                Gson gson = new Gson();
                Toast.makeText(activity, "cadastrado ", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activity, ListarPropriedadeActivity.class);
                intent.putExtra("user",user);
                activity.startActivity(intent);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String strings = new String(responseBody);
                Toast.makeText(activity, " status "+statusCode+" "+strings+" "+headers, Toast.LENGTH_SHORT).show();
            }
        });
        }else{
            client.put(this.activity, Utils.URL+"/api/propriedade/update/"+this.propriedade.getId(),entity,"application/json",new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String strings = new String(responseBody);
                    Gson gson = new Gson();
                    Toast.makeText(activity, "atualizado ", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(activity, ListarPropriedadeActivity.class);
                    intent.putExtra("user",user);
                    activity.startActivity(intent);
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    String strings = new String(responseBody);
                    Toast.makeText(activity, " status "+statusCode+" "+strings+" "+headers, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
