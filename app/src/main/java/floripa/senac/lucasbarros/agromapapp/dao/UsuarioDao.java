package floripa.senac.lucasbarros.agromapapp.dao;

import android.content.Context;

import floripa.senac.lucasbarros.agromapapp.model.User;

import floripa.senac.lucasbarros.agromapapp.dao.factory.DaoHelper;

public class UsuarioDao extends DaoHelper<User> {
    public UsuarioDao(Context context) {
        super(context, User.class);
    }
}
