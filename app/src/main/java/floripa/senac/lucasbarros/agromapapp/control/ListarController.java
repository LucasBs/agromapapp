package floripa.senac.lucasbarros.agromapapp.control;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import floripa.senac.lucasbarros.agromapapp.R;
import floripa.senac.lucasbarros.agromapapp.Utils.Utils;
import floripa.senac.lucasbarros.agromapapp.model.Propriedade;
import floripa.senac.lucasbarros.agromapapp.model.User;
import floripa.senac.lucasbarros.agromapapp.view.PerfilActivity;

public class ListarController {

    private ListView listView;
    private Activity activity;
    private ArrayAdapter<Propriedade> adapter;
    private User user;
    private AsyncHttpClient client = new AsyncHttpClient();

    public ListarController(Activity activity) {
        this.activity = activity;
        init();
        loadlist();
    }

    private void init(){
        this.listView = activity.findViewById(R.id.lista);

        this.user = (User) this.activity.getIntent().getExtras().getSerializable("user");
        client.addHeader("user-agent","Mozilla Chrome");
        clickItem();
    }

    private void loadlist(){


        client.get(Utils.URL+"/api/propriedade/user/"+this.user.getId(),new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String strings = new String(responseBody);
                Gson gson = new Gson();
                Type collectionType = new TypeToken<Collection<Propriedade>>(){}.getType();
                List<Propriedade> propriedades =  gson.fromJson(strings,collectionType);
                adapter = new ArrayAdapter<>(activity,android.R.layout.simple_list_item_1,propriedades);
                listView.setAdapter(adapter);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String strings = new String(responseBody);
                Toast.makeText(activity, " status "+statusCode+" "+strings+" "+headers, Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void cadastrar(){
        Intent it = new Intent(activity, PerfilActivity.class);
        it.putExtra("userId", user);
        activity.startActivity(it);
    }


    public  void clickItem(){
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, View view, final int position, long id) {

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setMessage("")
                        .setTitle("Opções")
                        .setPositiveButton("Editar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {  Intent it = new Intent(activity, PerfilActivity.class);
                                it.putExtra("userId", user);
                                Propriedade item = (Propriedade) parent.getItemAtPosition(position);
                                it.putExtra("propriedade",item);
                                activity.startActivity(it);
                            }
                        })
                        .setNegativeButton("Deletar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                                builder.setMessage("Deseja realmente deletar?")
                                        .setTitle("Deletar")
                                        .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {  Intent it = new Intent(activity, PerfilActivity.class);
                                                Propriedade item = (Propriedade) parent.getItemAtPosition(position);

                                                client.delete(Utils.URL+"/api/propriedade/remove/"+item.getId(),new AsyncHttpResponseHandler() {
                                                    @Override
                                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                                        loadlist();
                                                        Toast.makeText(activity, "Deletado", Toast.LENGTH_SHORT).show();
                                                    }
                                                    @Override
                                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                                        String strings = new String(responseBody);
                                                        Toast.makeText(activity, " status "+statusCode+" "+strings+" "+headers, Toast.LENGTH_SHORT).show();
                                                    }
                                                });

                                            }
                                        })
                                        .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                            }
                                        });
                                // Create the AlertDialog object and return it
                                builder.show();
                            }
                        });
                // Create the AlertDialog object and return it
                builder.show();

            }
        });
    }
}
